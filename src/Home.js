import ArticlesFeed from "./Components/HomePage/ArticleFeed";
import HeroBanner from "./Components/HomePage/HeroBanner";
import NavBar from "./Components/HomePage/NavBar";

import React, { Component } from "react";
import AfterLoginNavBar from "./Components/HomePageAfterLogin/AfterLoginNavbar";

class Home extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <div className="fixed w-full">
          {this.props.isLoggedIn ? (
            <AfterLoginNavBar user={this.props.userData} />
          ) : (
            <NavBar />
          )}
        </div>
        {this.props.isLoggedIn ? <div className="h-10"></div> : <HeroBanner />}
        {/* <HeroBanner /> */}
        <ArticlesFeed isLoggedIn={this.props.isLoggedIn} />
      </>
    );
  }
}

export default Home;

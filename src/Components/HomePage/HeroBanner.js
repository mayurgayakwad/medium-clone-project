import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import medium from '../../medium.svg'
class HeroBanner extends Component {
  render() {
    return (
      <div className="h-[575px ] lg:h-[538px] bg-[#ffc107] flex justify-center border-b-[1px] border-black">
        <div className="w-5/6 lg:w-4/6 flex ">
          <div className=" py-[125px] w-[550px] tracking-tighter">
            <h2 className="text-[75px] leading-[80px] mb-8 lg:leading-[120px] lg:mb-6 lg:w-full lg:text-[106px] font-serif font-normal ">
              Stay curious.
            </h2>
            <h3 className="text-[24px] leading-6 lg:w-[400px] mb-[35px] font-normal">
              Discover stories, thinking, and expertise from writers on any
              topic.
            </h3>
            <NavLink to={"/register"}>
              {" "}
              <button className="text-[20px] mt-4 bg-black text-white w-[213px] h-11 rounded-[30px]">
                Start reading
              </button>
            </NavLink>
          </div>
          <div className="hidden md:flex hidden-sm flex-wrap flex-col-reverse" style={{marginLeft:100}}><img src={medium}></img></div>
          <div className="h-20">
            <></>
          </div>
        </div>
      </div>
    );
  }
}

export default HeroBanner;

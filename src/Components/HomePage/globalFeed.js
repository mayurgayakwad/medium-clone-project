import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Loader from "../Loader/feedLoader";

class GlobalFeed extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }
  render() {
    if (!this.props.data) {
      return (
        <>
          <div className="w-full h-full flex justify-center items-center">
            <div style={{ marginTop: this.props.isLoggedIn ? '250px' : '-250px' }}>
              <Loader />
            </div>
          </div>
        </>
      );
    }
    return (
      <>
        <div className="w-full">
          {this.props.data.articles.map((info, index) => (
            <>
              <div
                key={`div1${index}`}
                className="flex gap-2 mt-6 items-center mb-2"
              >
                <img
                  key={`img1${index}`}
                  alt="Not Found"
                  src={info.author.image}
                  className="lg:w-8  lg:h-8 rounded-[50%] w-6 h-6"
                />{" "}
                <span
                  key={`span1${index}`}
                  className="font-semibold text-sm lg:text-lg"
                >
                  {info.author.username}
                </span>
              </div>
              <div key={`div2${index}`} className="flex lg:gap-10 gap-4">
                <NavLink to={`articles/${info.slug}`}>
                  <div key={`div3${index}`} className="flex flex-col">
                    <h4
                      ey={`h4${index}`}
                      className="text-[16px] lg:text-[22px] font-semibold w-[200px] lg:w-[550px] lg:hidden"
                    >
                      {`${info.title.substring(0, 40)}...`}
                    </h4>
                    <h4
                      ey={`h4${index}`}
                      className="text-[16px] lg:text-[22px] hidden font-semibold w-[550px] lg:block"
                    >
                      {`${info.title}`}
                    </h4>
                    <p
                      key={`p1${index}`}
                      className="text-lg text-gray-500 leading-1 hidden lg:block"
                    >
                      {`${info.description.substring(0, 110)} read more...`}
                    </p>
                    <div
                      key={`div4${index}`}
                      className="flex mt-2 gap-2 text-slate-400 items-center text-[9px] lg:text-[16px]"
                    >
                      <p key={`p2${index}`}>
                        {`Dec ${info.createdAt.substring(8, 10)}`}
                      </p>
                      &#x2022;
                      <p key={`p3${index}`}>6 min read</p>&#x2022;
                      <p
                        key={`p4${index}`}
                        className="py-[2px] px-3  bg-[#eeeee4] rounded-2xl"
                      >
                        {info.tagList.filter((tag, index) => index === 0)}
                      </p>
                      <svg
                        key={`svg${index}`}
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                      >
                        <path
                          d="M12.4 12.77l-1.81 4.99a.63.63 0 0 1-1.18 0l-1.8-4.99a.63.63 0 0 0-.38-.37l-4.99-1.81a.62.62 0 0 1 0-1.18l4.99-1.8a.63.63 0 0 0 .37-.38l1.81-4.99a.63.63 0 0 1 1.18 0l1.8 4.99a.63.63 0 0 0 .38.37l4.99 1.81a.63.63 0 0 1 0 1.18l-4.99 1.8a.63.63 0 0 0-.37.38z"
                          fill="#FFC017"
                        ></path>
                      </svg>
                    </div>
                  </div>
                </NavLink>
                <div key={`div6${index}`} className="flex">
                  <img
                    key={`img3${index}`}
                    alt="Not Found"
                    src="https://images.unsplash.com/photo-1455390582262-044cdead277a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1073&q=80"
                    className=" w-[300px]"
                  />
                </div>
              </div>
            </>
          ))}
        </div>
      </>
    );
  }
}

export default GlobalFeed;

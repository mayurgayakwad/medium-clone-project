import React from "react";
import "../Loader/loaderStyle.css";
class Loader extends React.Component {
  render() {
    return (
      <>
        <div class="feedLoader">
          <div class="circle"></div>
          <div class="circle"></div>
          <div class="circle"></div>
          <div class="circle"></div>
        </div>
      </>
    );
  }
}
export default Loader;

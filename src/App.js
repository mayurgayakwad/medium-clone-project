import React, { Component } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import IndividualArticle from "./Components/Individual/IndividualArticle";
import Loader from "./Components/Loader/loader";
import Login from "./Components/Login/login";
import NewPost from "./Components/NewPost/NewPost";
import Error from "./Components/ErrorPage/Error";
import Profile from "./Components/Profile/Profile";
import Setting from "./Components/Setting/Setting";
import SignUp from "./Components/SignUp/signup";
import Home from "./Home";
// import HomePageAfterLogin from "./HomePageAfterLogin";

class App extends Component {
  constructor() {
    super();
    this.state = {
      isLoggedIn: false,
      isUserLogouting: false,
      user: null,
      isVerifying: true,
    };
  }
  componentDidMount() {
    let storageKey = localStorage.token;
    this.setState({isVerifying: true });
    if (storageKey) {
      fetch("https://api.realworld.io/api/user", {
        method: "GET",
        headers: {
          authorization: `Token ${storageKey}`,
        },
      })
        .then((res) => res.json())
        .then(({ user }) => {
          this.setState({ isLoggedIn: true, user, isVerifying: false });
          console.log(
            "condtion true",
            localStorage.app_user,
            localStorage.token
          );
          localStorage.setItem("token", user.token);
          localStorage.setItem("app_user", JSON.stringify(user));
        });
    } else {
      this.setState({ isVerifying: false });
    }
  }
  updateUser = (user) => {
    if (user) {
      this.setState({ isLoggedIn: true, user, isVerifying: false });
      localStorage.setItem("token", user.user.token);
      localStorage.setItem("app_user", JSON.stringify(user.user));
    }
  };
  logoutUser = () => {
    console.log("button click");
    this.setState({
      isLoggedIn: false,
      isUserLogouting: true,
      user: null,
      isVerifying: false,
    });
    localStorage.clear();
    window.location.replace("/");
  };

  render() {
    if (this.state.isVerifying || this.props.isUserLogouting) {
      return (
        <>
          <div className="mt-[40vh]">
            <Loader />
          </div>
        </>
      );
    }

    return (
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Home
                isLoggedIn={this.state.isLoggedIn}
                user={this.props.user}
                userData={this.state.user}
              />
            }
          />
          <Route
            path="/login"
            element={<Login updateUser={this.updateUser} />}
          />
          <Route
            path="/register"
            element={<SignUp updateUser={this.updateUser} />}
          />
          {/* <Route path="/individualArticle" element={<IndividualArticle />} /> */}
          <Route path="/New-post" element={<NewPost />} />
          <Route path="/profile" element={<Profile />} />
          <Route
            path="/setting"
            element={<Setting isUserLogouting = {this.state.isUserLogouting} logoutUser={this.logoutUser} />}
          />
          <Route
            path="/articles/:slug"
            element={<IndividualArticle isLoggedIn={this.state.isLoggedIn} />}
          />
          <Route
            path="profile/articles/:slug"
            element={<IndividualArticle isLoggedIn={this.state.isLoggedIn} />}
          />
          <Route path="*" element={<Error />} />

          {/* <Route path="*" element={<Login />} /> */}
        </Routes>
      </BrowserRouter>
    );
  }
}

export default App;

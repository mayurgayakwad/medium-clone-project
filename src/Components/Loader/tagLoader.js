import React, { Component } from "react";
import "../Loader/loaderStyle.css";

class TagLoader extends Component {
  render() {
    return <div className="loader mt-10"></div>;
  }
}

export default TagLoader;

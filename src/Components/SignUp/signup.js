import React, { Component } from "react";
import { json, NavLink } from "react-router-dom";
import NavBar from "../HomePage/NavBar";

//this email add exist
// username: "Mayur",
// email: "mayurgyakwad12@gmail.com",
// password: "mayurgayak",
class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      usernameError: "",
      emailError: "",
      passwordError: "",
      errors: null,
      user: null,
      isLoading: false,
    };
    // this.localStorage.setItem("username", json.stringify(this.state.username));
  }

  validat = () => {
    let emailError = "";
    let passwordError = "";
    let usernameError = "";
    let errmsg = true;
    if (!this.state.username) {
      usernameError = "Username required";
      // this.setState({ usernameError });
      // return false;
    } else {
      usernameError = "";
    }
    if (usernameError) {
      this.setState({ usernameError });
      errmsg = false;
    }
    if (!this.state.email) {
      emailError = "Valid Email Required ";
    }
    if (emailError) {
      this.setState({ emailError });
      errmsg = false;
    }
    if (this.state.password.length === 0) {
      passwordError = "Passwords must not be empty.";
      this.setState({ passwordError });
      errmsg = false;
    } else if (this.state.password.length <= 8) {
      passwordError = "At least 8 characters must be in your password.";
      this.setState({ passwordError });
      errmsg = false;
    }
    if (passwordError) {
      this.setState({ passwordError });
      errmsg = false;
    }
    return errmsg;
  };
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  signup = () => {
    const { username, email, password } = this.state;
    this.setState({ isLoading: true });
    fetch("https://api.realworld.io/api/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        user: {
          username,
          email,
          password,
        },
      }),
    }).then((res) => {
      // console.log(res);
      if (!res.ok) {
        // console.log(res);
        res.json().then((error) => {
          console.log("Email Already Exist");
          this.setState({
            errors: error,
            isLoading: false,
          });
        });
      } else {
        res.json().then((users) => {
          // console.log(users);
          window.location.replace("/");
          this.setState({
            user: users,
          });
          this.props.updateUser(users);
        });
      }
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const isValid = this.validat();
    if (isValid) {
      console.log({
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
      });
    }
    if (isValid) {
      this.signup();
    }
  };
  render() {
    // console.log(this.state.user ? "" : this.state.user);
    return (
      <>
        <NavBar />
        <form onSubmit={(e) => this.handleSubmit(e)}>
          <div className="flex justify-center">
            <div className="lg:w-[500px] flex flex-col mt-10 relative">
              <h1 className="text-[2.5rem] text-inherit text-center">
                Sign Up
              </h1>
              <NavLink
                to={"/login"}
                className="text-green-500 text-[15px]  text-center inline-block hover:underline decoration-solid"
              >
                Have an account?
              </NavLink>
              <div className="flex flex-col gap-5 mt-4">
                {this.state.errors &&
                this.state.errors.errors.username &&
                this.state.errors.errors.email ? (
                  <li className="text-[#bb5353] ml-2">
                    Username & Email already exists *
                  </li>
                ) : this.state.errors && this.state.errors.errors.email ? (
                  <li className="text-[#bb5353] ml-2">Email already exists*</li>
                ) : this.state.errors && this.state.errors.errors.username ? (
                  <li className="text-[#bb5353] ml-2">
                    Username already exists *
                  </li>
                ) : (
                  ""
                )}
                <input
                  type="text"
                  placeholder="Username"
                  name="username"
                  value={this.state.username}
                  onChange={(e) => this.handleChange(e)}
                  className="h-14 lg:w-[500px] text-[20px] px-5 border border-slate-200 items-center rounded-lg"
                />
                <div className="text-[#bb5353] ml-2">
                  {this.state.usernameError}
                </div>
                <input
                  type="email"
                  placeholder="Email"
                  name="email"
                  value={this.state.email}
                  onChange={(e) => this.handleChange(e)}
                  className="h-14 lg:w-[500px] px-5 text-[20px] border border-slate-200 items-center rounded-lg"
                />
                <div className="text-[#bb5353] ml-2">
                  {this.state.emailError}
                </div>
                <input
                  type="password"
                  placeholder="Password"
                  name="password"
                  value={this.state.password}
                  onChange={(e) => this.handleChange(e)}
                  className="h-14 px-5 border text-[20px] border-slate-200 lg:w-[500px] rounded-lg"
                />
                <div className="text-[#bb5353] ml-2">
                  {this.state.passwordError}
                </div>{" "}
                <button
                  type="submit"
                  className="bg-[#ffc107] shadow-lg px-6 py-3 text-xl rounded-lg float-right self-end"
                  disabled = {this.state.isLoading}
                >
                  {this.state.isLoading ? 'Loading... ' : 'Sign Up'}
                </button>
              </div>
            </div>
          </div>
        </form>
      </>
    );
  }
}

export default SignUp;

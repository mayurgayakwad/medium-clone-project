// import React, { Component } from "react";
// import { NavLink } from "react-router-dom";
// import NavBar from "../HomePage/NavBar";

// class IndividualArticle extends Component {
//   render() {
//     console.log(this.props.user);
//     return (
//       <>
//         <NavBar />
//         <div className="h-56 bg-slate-100/90 flex justify-center">
//           <div className="w-4/6 flex flex-col gap-4 py-8 items-center">
//             <img
//               alt="Not Found"
//               src="https://images.unsplash.com/photo-1672929280546-af55a5554fd8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1005&q=80"
//               className="w-24 h-24 rounded-[50%]"
//             />
//             <p className="font-semibold text-2xl">Mayur Gayakwad</p>
//             <button className="self-end text-gray-400 text-sm  border-[2px] border-gray-200 px-1 py-1">
//               + Follow Mayur Gayakwad
//             </button>
//           </div>
//         </div>
//         <div className="flex justify-center">
//           <div className="flex flex-col w-4/6 mt-12 ">
//             <h1 className="pl-5 pb-2 text-[#5cb85c] border-b-2 w-[125px] border-[#5cb85c]">
//               Global Feed
//             </h1>
//             <hr className="" />
//             <div className="snap-mandatory snap-y h-[1000px]">
//               <div className="flex gap-2 mt-6 items-center mb-4">
//                 <img
//                   alt="Not Found"
//                   src="https://images.unsplash.com/photo-1672929280546-af55a5554fd8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1005&q=80"
//                   className="w-8 h-8 rounded-[50%]"
//                 />
//                 <NavLink to={"/individualArticle"}>
//                   {" "}
//                   <span className="font-semibold">Mayur Gayakwad</span>
//                 </NavLink>
//               </div>
//               <div className="flex gap-10">
//                 <div className="flex flex-col">
//                   <h4 className="text-[22px] font-semibold">
//                     How senior product managers think differently
//                   </h4>
//                   <p className="text-lg text-gray-500 leading-1">
//                     A deep look into product management toolkit to advance your
//                     thinking and become a better PM
//                   </p>
//                   <div className="flex gap-2 text-slate-400 items-center">
//                     <p>date</p>&#x2022;
//                     <p>6 min read</p>&#x2022;
//                     <p className="py-1 px-3 bg-[#eeeee4] rounded-2xl">
//                       History
//                     </p>
//                     <svg width="20" height="20" viewBox="0 0 20 20" fill="none">
//                       <path
//                         d="M12.4 12.77l-1.81 4.99a.63.63 0 0 1-1.18 0l-1.8-4.99a.63.63 0 0 0-.38-.37l-4.99-1.81a.62.62 0 0 1 0-1.18l4.99-1.8a.63.63 0 0 0 .37-.38l1.81-4.99a.63.63 0 0 1 1.18 0l1.8 4.99a.63.63 0 0 0 .38.37l4.99 1.81a.63.63 0 0 1 0 1.18l-4.99 1.8a.63.63 0 0 0-.37.38z"
//                         fill="#FFC017"
//                       ></path>
//                     </svg>
//                   </div>
//                 </div>
//                 <div className="flex">
//                   <img
//                     alt="Not Found"
//                     src="https://images.unsplash.com/photo-1672929280546-af55a5554fd8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1005&q=80"
//                     className="w-[150px]"
//                   />
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </>
//     );
//   }
// }

// export default IndividualArticle;

import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { NavLink, useNavigate } from "react-router-dom";
import Loader from "../Loader/feedLoader";
import NavBar from "../HomePage/NavBar";
import AfterLoginNavBar from "../HomePageAfterLogin/AfterLoginNavbar";
import RefreshPage from "../RefreshPage/refreshPage";

const IndividualArticle = (props) => {
  const intial = useParams();
  const { slug } = intial;
  const [individualArticle, setIndividualArticle] = useState(null);
  const fetchingData = () => {
    fetch(`https://api.realworld.io/api/articles/${slug}`)
      .then((res) => res.json())
      .then((data) => setIndividualArticle(data));
  };
  useEffect(() => {
    fetchingData();
  }, []);

  const isUserAvailable = localStorage.getItem("app_user")
  const localStorageData = isUserAvailable && JSON.parse(isUserAvailable);
  let authoreCompare2 = localStorageData?.username;
  const navigator = useNavigate();
  const DeleteArticle = () => {
    let storageKey = localStorageData.token;
    console.log("Delete");
    const headers = {
      "Content-Type": "application/json; charset=utf-8",
      authorization: `Token ${storageKey}`,
    };
    fetch(`https://api.realworld.io/api/articles/${slug}`, {
      method: "DELETE",
      headers: headers,
    });
    navigator("/");
  };
  if (!localStorageData && props.isLoggedIn) {
    return (
      <RefreshPage></RefreshPage>
    )
  }
  return (
    <>
      {props.isLoggedIn ? <AfterLoginNavBar /> : <NavBar />}
      <div className=" pt-20 max-sm:pt-14">
        {individualArticle == null ? (
          <div className="w-full h-full flex justify-center items-center">
            <div style={{ marginTop: "250px" }}>
              <Loader />
            </div>
          </div>
        ) : (
          <div className=" flex flex-col justify-center">
            <div className="w-4/5 lg:w-2/5 flex items-center self-center">
              <img
                className="rounded-[50%] "
                alt="Not-Found"
                src="https://miro.medium.com/fit/c/32/32/1*sHhtYhaCe2Uc3IU0IgKwIQ.png"
              />{" "}
              <span className="text-gray-400 ml-4 hidden lg:block">
                Published in 6 Min Read
              </span>
              {individualArticle.article.author.username == authoreCompare2 ? (
                <div className="ml-20 flex flex-row gap-3">
                  <button className="px-3 items-center text-sm flex justify-center border border-black py-1 px-2 rounded-lg hover:border-blue-400">
                    {/* <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 30 30"
                      width="30px"
                      height="30px"
                    >
                      {" "}
                      <path d="M 22.828125 3 C 22.316375 3 21.804562 3.1954375 21.414062 3.5859375 L 19 6 L 24 11 L 26.414062 8.5859375 C 27.195062 7.8049375 27.195062 6.5388125 26.414062 5.7578125 L 24.242188 3.5859375 C 23.851688 3.1954375 23.339875 3 22.828125 3 z M 17 8 L 5.2597656 19.740234 C 5.2597656 19.740234 6.1775313 19.658 6.5195312 20 C 6.8615312 20.342 6.58 22.58 7 23 C 7.42 23.42 9.6438906 23.124359 9.9628906 23.443359 C 10.281891 23.762359 10.259766 24.740234 10.259766 24.740234 L 22 13 L 17 8 z M 4 23 L 3.0566406 25.671875 A 1 1 0 0 0 3 26 A 1 1 0 0 0 4 27 A 1 1 0 0 0 4.328125 26.943359 A 1 1 0 0 0 4.3378906 26.939453 L 4.3632812 26.931641 A 1 1 0 0 0 4.3691406 26.927734 L 7 26 L 5.5 24.5 L 4 23 z" />
                    </svg> */}
                    Edit Article
                  </button>
                  <button
                    onClick={DeleteArticle}
                    className="px-3 items-center text-sm flex justify-center border border-black py-1 px-2 rounded-lg hover:border-blue-400"
                  >
                    {/* <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 30 30"
                      width="30px"
                      height="30px"
                    >
                      <path d="M 13 3 A 1.0001 1.0001 0 0 0 11.986328 4 L 6 4 A 1.0001 1.0001 0 1 0 6 6 L 24 6 A 1.0001 1.0001 0 1 0 24 4 L 18.013672 4 A 1.0001 1.0001 0 0 0 17 3 L 13 3 z M 6 8 L 6 24 C 6 25.105 6.895 26 8 26 L 22 26 C 23.105 26 24 25.105 24 24 L 24 8 L 6 8 z" />
                    </svg>{" "} */}
                    Delete Article
                  </button>
                </div>
              ) : (
                ""
              )}
            </div>
            <hr className="self-center mt-4 w-4/5 lg:w-2/5" />
            <div className="flex lg:justify-center lg:w-4/6 ml-4 mt-6">
              <img
                alt="Not Found"
                src={individualArticle.article.author.image}
                className="rounded-[50%] w-12"
              />
              <div className="">
                <h2 className="ml-3 text-[16px]">
                  {individualArticle.article.author.username}
                </h2>
                <span className="ml-3">
                  {`${individualArticle.article.createdAt.slice(0, 10)}`}
                </span>
              </div>
            </div>
            <div className="mt-8 flex justify-center">
              <img
                className="w-4/6 lg:w-2/5"
                alt="Not-Found"
                src="https://images.unsplash.com/photo-1455390582262-044cdead277a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1073&q=80"
              />
            </div>
            <div className="self-center  lg:ml-0 lg:text-[32px] font-semibold  lg:w-2/5 lg:leading-10 my-6">
              {individualArticle.article.title}
            </div>
            <div className="  flex justify-center">
              <div className="w-2/5 text-xl font-serif text-[#292929] pb-6 max-sm:w-5/6 max-sm:text-sm ">
                <p>
                  {individualArticle.article.description.split("\\n").join(" ")}
                </p>
              </div>
            </div>
            <hr className="" />
            <div className=" mt-10 flex justify-center">
              {props.isLoggedIn ? (
                <div className="w-5/6 lg:w-3/6 px-10 mb-10 ">
                  <textarea
                    placeholder="Add Your Comment"
                    name="for Comment"
                    className="text-xl placeholder:text-lg mb-4 placeholder:italic max-sm:w-6/6 placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-6 shadow-sm focus:outline-none focus:ring-green-400 focus:ring-1"
                    rows="3"
                  ></textarea>
                  <button className=" p-4 bg-[#5cb85c] text-white text-xl shadow-lg rounded float-right">
                    Post Comment
                  </button>
                </div>
              ) : (
                <p className=" mb-10">
                  <NavLink to="/login">
                    <span className=" text-orange-500 cursor-pointer underline">
                      Sign In
                    </span>
                  </NavLink>{" "}
                  or{" "}
                  <NavLink to="/register">
                    <span className="text-orange-500 cursor-pointer underline">
                      Sign Up
                    </span>
                  </NavLink>{" "}
                  To Comment on this post.
                </p>
              )}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default IndividualArticle;

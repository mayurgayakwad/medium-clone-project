import React, { Component } from "react";
import AfterLoginNavBar from "../HomePageAfterLogin/AfterLoginNavbar";
import "../Setting/settingCss.css";
import RefreshPage from "../RefreshPage/refreshPage";
import Loader from "../Loader/loader";
class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // image: "",
      username: "",
      shortBio: "",
      email: "",
      password: "",
      token: "",
      BioError: "",
      ImageError: "",
      isDataUpdating: false,
      dataUpdated: false,
      errors: null,
    };
  }
  update = () => {
    let storageKey = this.state.token;
    const { image, username, shortBio: bio, email, password } = this.state;
    this.setState({ isDataUpdating: true });
    const headers = {
      "Content-Type": "application/json; charset=utf-8",
      authorization: `Token ${storageKey}`,
    };

    fetch("https://api.realworld.io/api/user", {
      method: "PUT",
      headers: headers,
      body: JSON.stringify({
        user: {
          image,
          username,
          bio,
          email,
          password,
        },
      }),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Failed to update data: " + response.statusText);
        }
      })
      .then((responseData) => {
        console.log({ responseData });
        localStorage.setItem("app_user", JSON.stringify(responseData.user));
        window.location.replace("/");
      })
      .catch((error) => {
        console.error(error);
        this.setState({
          isDataUpdating: false,
        });
      });
  };

  validat = () => {
    let BioError = "";
    let ImageError = "";
    let errmsg = true;
    if (!this.state.bio && !this.state.shortBio) {
      BioError = "Bio cannot be Empty";
    } else {
      BioError = "";
    }
    if (BioError) {
      this.setState({ BioError });
      errmsg = false;
    }
    if (!this.state.image) {
      ImageError = "Image Field cannot be Empty";
    } else {
      ImageError = "";
    }
    if (ImageError) {
      this.setState({ ImageError });
      errmsg = false;
    }
    return errmsg;
  };
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentDidMount() {
    const dataFromLocal = JSON.parse(localStorage.getItem("app_user"));
    console.log(dataFromLocal);
    if (dataFromLocal) {
      this.setState({
        username: dataFromLocal.username,
        image: dataFromLocal.image,
        shortBio: dataFromLocal.bio,
        email: dataFromLocal.email,
        token: dataFromLocal.token,
      });
    }
  }
  handlerSubmit = (e) => {
    e.preventDefault();
    const isValid = this.validat();
    if (isValid) {
      this.update();
    }
  };
  render() {
    if (this.props.isUserLogouting) {
      return (
        <div class="flex items-center justify-center h-screen">
          <div>
            <Loader />
          </div>
        </div>
      );
    }
    if (!JSON.parse(localStorage.getItem("app_user"))) {
      return <RefreshPage></RefreshPage>;
    }
    return (
      <>
        <AfterLoginNavBar />
        <div className="flex mt-8 mb-8 lg:mb-0 mr-12 float-right">
          <button onClick={this.props.logoutUser} className="btn">
            Click here to logout
          </button>
        </div>
        <center>
          <form onSubmit={(e) => this.handlerSubmit(e)}>
            <div className="lg:w-[50%] lg:mt-4 mx-8 my-auto p-4 ">
              <div className=" flex flex-col w-full gap-2 lg:w-[60%] content-none">
                <h2 className="text-2xl lg:text-4xl  mt-2 text-center mb-8">{`Hi, ${this.state.username}`}</h2>

                <input
                  className="h-14 lg:w-full px-5 text-[20px] border cursor-pointer border-slate-200 items-center rounded-lg  hover:border-indigo-300 shadow-sm  placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  focus:ring-1"
                  name="image"
                  type="text"
                  placeholder="URL of profile picture"
                  value={this.state.image}
                  onChange={(e) => this.handleChange(e)}
                  autoComplete={"current-img"}
                />
                <p>{this.state.image ? "" : this.state.ImageError}</p>
                <input
                  name="username"
                  disabled
                  className="h-14 w-full px-5 text-[20px] border cursor-pointer border-slate-200 items-center rounded-lg hover:border-indigo-300 cursor-not-allowed"
                  type="text"
                  placeholder={`username`}
                  //   value={username}
                  value={this.state.username}
                  onChange={(e) => this.handleChange(e)}
                  autoComplete={"current-username"}
                />
                <textarea
                  name="shortBio"
                  className="h-52 w-full p-5 text-[20px] cursor-pointer  items-center rounded-lg pt-5 hover:border-indigo-300 border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block  focus:ring-1"
                  placeholder={`Short bio about you`}
                  autoComplete={"current-bio"}
                  value={this.state.shortBio}
                  onChange={(e) => this.handleChange(e)}
                ></textarea>
                <p>{this.state.bio ? "" : this.state.BioError}</p>

                <input
                  name="email"
                  disabled
                  className="h-14 w-full px-5 text-[20px] border cursor-pointer border-slate-200 items-center rounded-lg hover:border-indigo-300 cursor-not-allowed"
                  type="email"
                  placeholder={`Email`}
                  value={this.state.email}
                  onChange={(e) => this.handleChange(e)}
                  autoComplete={"current-email"}
                />
                {/* <input
                  name="password"
                  className="h-14 w-full px-5 text-[20px] border cursor-pointer border-slate-200 items-center rounded-lg hover:border-indigo-300"
                  type="password"
                  placeholder={`New Password`}
                  onChange={(e) => this.handleChange(e)}
                  autoComplete={"current-password"}
                /> */}
                <input
                  type="submit"
                  disabled = {this.state.isLoading}
                  value={
                    this.state.isDataUpdating ? "Loading..." : "Update Settings"
                  }
                  className="bg-[#ffc107] px-6 py-3 text-xl shadow-lg sh shadow-slate-300 rounded-lg cursor-pointer float-right self-end"
                />
              </div>
              <hr className="my-10 w-[60%]" />
              <p className=" text-center text-2xl">
                {this.state.dataUpdated
                  ? "Data updated successfully! ✅"
                  : null}
              </p>
            </div>
          </form>
        </center>
      </>
    );
  }
}

export default Setting;

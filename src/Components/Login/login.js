import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import NavBar from "../HomePage/NavBar";

// let email = "";
// let password = "";
// email: "mayurgyakwad12@gmail.com",
// password: "mayurgayak",
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      emailError: "",
      password: "",
      passwordError: "",
      errors: null,
      user: null,
      showPassword: false,
      isLoading: false,
    };
  }
  validat = () => {
    let emailError = "";
    let passwordError = "";
    let errorState = true;
    if (!this.state.email) {
      emailError = "Valid Email Required ";
    }
    if (emailError) {
      this.setState({ emailError });
      errorState = false;
    }
    if (!this.state.password) {
      passwordError = "Please Enter Your Password";
      this.setState({ passwordError });
      errorState = false;
    }
    if (passwordError) {
      this.setState({ passwordError });
      errorState = false;
    }
    return errorState;
  };

  login = () => {
    let { email, password } = this.state;
    this.setState({ isLoading: true });
    fetch("https://api.realworld.io/api/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        user: {
          email,
          password,
        },
      }),
    }).then((res) => {
      console.log(res);
      if (!res.ok) {
        console.log("Email & Password Invalid");
        res.json().then((error) => {
          return this.setState({
            errors: error,
            isLoading: false,
          });
        });
      } else {
        res.json().then((users) => {
          console.log(users);
          this.props.updateUser(users);
          window.location.replace("/");

          return this.setState({
            user: users,
          });
        });
      }
    });
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  togglePasswordVisibility = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const isValid = this.validat();
    if (isValid) {
      this.login();
    }
  };

  render() {
    return (
      <>
        <NavBar />
        <form onSubmit={(e) => this.handleSubmit(e)}>
          <div className="flex justify-center">
            <div className=" lg:w-[500px] flex flex-col mt-10">
              <h1 className="text-[2.5rem] text-inherit text-center">
                Sign In
              </h1>
              <NavLink
                to={"/register"}
                className="text-green-500 text-[15px]  text-center inline-block hover:underline decoration-solid"
              >
                Need an account?
              </NavLink>
              <div className=" flex flex-col gap-5 mt-4">
                {this.state.errors ? (
                  <li className="text-[#bb5353] ml-2">
                    wrong email or password
                  </li>
                ) : (
                  ""
                )}
                <input
                  type="email"
                  placeholder="Email"
                  name="email"
                  value={this.state.email}
                  onChange={(e) => this.handleChange(e)}
                  className="h-14 w-full lg:w-[500px] px-5 text-[20px] border border-slate-200 items-center rounded-lg"
                />
                <div className="text-[#bb5353] ml-2">
                  {this.state.emailError}
                </div>
                <div className="relative">
                  <input
                    type={this.state.showPassword ? "text" : "password"}
                    placeholder="Password"
                    name="password"
                    value={this.state.password}
                    onChange={(e) => this.handleChange(e)}
                    className="h-14 px-5 border text-[20px] border-slate-200 lg:w-[500px] rounded-lg pr-12"
                  />
                  {this.state.password && (
                    <div
                      onClick={() => this.togglePasswordVisibility()}
                      className="absolute top-1/2 right-4 transform -translate-y-1/2 cursor-pointer text-green-500"
                    >
                      {this.state.showPassword ? "hide" : "show"}
                    </div>
                  )}
                </div>
                <div className="text-[#bb5353] ml-2">
                  {this.state.passwordError}
                </div>
                <button
                  type="submit"
                  className="bg-[#ffc107] shadow-lg px-6 py-3 text-xl rounded-lg self-end"
                  disabled = {this.state.isLoading}
                >
                  {this.state.isLoading ? "Loading... " : "Sign In"}
                </button>
              </div>
            </div>
          </div>
        </form>
      </>
    );
  }
}

export default Login;

import React, { Component } from "react";
// import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import Loader from "../Loader/loader";
import TagLoader from "../Loader/tagLoader";
import GlobalFeed from "./globalFeed";
import YourFeed from "./YourFeed";

class ArticlesFeed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      globalFeedData: null,
      yourFeedData: null,
      tag: null,
      activeFeed: this.props.isLoggedIn,
    };
  }

  toggleFeeds = () => {
    this.setState({
      activeFeed: !this.state.activeFeed,
    });
  };

  fechingGlobalFeed = () => {
    let url = `https://api.realworld.io/api/articles`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => this.setState(() => (this.state.globalFeedData = data)));
  };

  fechingTagData = () => {
    let url = `https://api.realworld.io/api/tags`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => this.setState(() => (this.state.tag = data)));
  };

  fechingYourFeed = () => {
    let storageKey = localStorage.token;
    const headers = {
      "Content-Type": "application/json; charset=utf-8",
      authorization: `Token ${storageKey}`,
    };
    let url = `https://api.realworld.io/api/articles?limit=10&offset=0`;
    fetch(url, {
      method: "GET",
      headers: headers,
    })
      .then((res) => res.json())
      .then((data) => this.setState(() => (this.state.yourFeedData = data)));
  };

  componentDidMount() {
    this.fechingTagData();
    this.fechingGlobalFeed();
    if (localStorage.token) {
      this.fechingYourFeed()
    }
  }
  render() {
    return (
      <>
        <div className="flex  lg:justify-center">
          <div className=" flex  lg:gap-20 lg:w-4/6 h-[550px] justify-center">
            <div className="flex-col al w-5/6 lg:w-4/6 mt-12 ">
              <div className="flex gap-2">
                {this.props.isLoggedIn && (
                  <button
                    onClick={() => this.toggleFeeds()}
                    className={
                      this.state.activeFeed
                        ? " pl-5 pb-2 text-[#5cb85c] border-b-2 px-5 border-[#5cb85c]"
                        : "pl-5 pb-2 text-black px-5 "
                    }
                  >
                    Your Feed
                  </button>
                )}
                <button
                  onClick={() => this.toggleFeeds()}
                  className={
                    this.state.activeFeed
                      ? "pl-5 pb-2 text-black  px-5 "
                      : " pl-5 pb-2 text-[#5cb85c] border-b-2 px-5 border-[#5cb85c]"
                  }
                >
                  Global Feed
                </button>
              </div>
              <hr className="hidden lg:block" />
              {this.state.activeFeed ? (
                <YourFeed data={this.state.yourFeedData} />
              ) : (
                  <GlobalFeed data={this.state.globalFeedData} isLoggedIn={this.props.isLoggedIn } />
              )}
            </div>
            <div className=" w-5/6 lg:w-2/6  mt-20 hidden lg:block">
              <h5 className="ml-1 text-[12px] font-bold tracking-[0.0083]">
                DISCOVER MORE OF WHAT MATTERS TO YOU
              </h5>
              {this.state.tag ? (
                this.state.tag.tags.map((tags, index) => (
                  <div
                    key={`div_tag${index}`}
                    className="mt-2 flex-wrap text-[#757575] inline-block text-[13px] font-normal"
                  >
                    <p
                      key={`p_tag${index}`}
                      className="py-[6px] px-[16px] m-1 border border-[[#eeeee4] rounded-[5px] inline-block"
                    >
                      {tags}
                    </p>
                  </div>
                ))
              ) : (
                <TagLoader />
              )}
              <hr className="mt-7 ml-1  " />
              <div
                key={"footer"}
                className="ml-1 py-6 lg:flex flex-wrap gap-6 text-sm text-[#757575] hidden  font-normal "
              >
                <p>Help</p>
                <p>Status</p>
                <p>Writers</p>
                <p>Blog</p>
                <p>Careers</p>
                <p>Privacy</p>
                <p>Terms</p>
                <p>About</p>
                <p>Text to speech</p>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
// function mapStateToProps(state) {
//   return {
//     data: state,
//   };
// }
export default ArticlesFeed;

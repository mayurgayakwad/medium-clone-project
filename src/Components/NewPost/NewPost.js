import React, { Component } from "react";
import AfterLoginNavBar from "../HomePageAfterLogin/AfterLoginNavbar";
import RefreshPage from "../RefreshPage/refreshPage";

class NewPost extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      body: "",
      tagList: "",
      description: "",
      createdAt: "",
      titleError: "",
      bodyError: "",
      tagListError: "",
      descriptionError: "",
      newPostCreated: false,
      updatingArtical: false,
    };
  }

  writePost = () => {
    let storageKey = localStorage.token;
    let { title, body, tagList, description } = this.state;
    tagList = tagList.split(",").map((tag) => tag.trim());
    this.setState({ updatingArtical: true });
    const headers = {
      "Content-Type": "application/json; charset=utf-8",
      authorization: `Token ${storageKey}`,
    };

    fetch("https://api.realworld.io/api/articles", {
      method: "POST",
      headers: headers,
      body: JSON.stringify({
        article: {
          title,
          description,
          body,
          tagList,
        },
      }),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          this.setState({ updatingArtical: true });
          throw new Error("Failed to update data: " + response.statusText);
        }
      })
      .then((responseData) => {
        this.setState({
          newPostCreated: true,
          title: responseData.article.title,
          description: responseData.article.description,
          body: responseData.article.body,
          tagList: responseData.article.tagList,
          createdAt: responseData.article.createdAt,
        });
        window.location.replace("/");
      })
      .catch((error) => {
        console.error(error);
      });
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  validat = () => {
    let titleError = "";
    let bodyError = "";
    let tagListError = "";
    let descriptionError = "";
    let errmsg = true;
    if (!this.state.title) {
      titleError = "Title cannot be Empty";
    } else {
      titleError = "";
    }
    if (titleError) {
      this.setState({ titleError });
      errmsg = false;
    }
    if (!this.state.body) {
      bodyError = "Body cannot be Empty";
    } else {
      bodyError = "";
    }
    if (bodyError) {
      this.setState({ bodyError });
      errmsg = false;
    }
    if (!this.state.description) {
      descriptionError = "Description cannot be Empty";
    } else {
      descriptionError = "";
    }
    if (descriptionError) {
      this.setState({ descriptionError });
      errmsg = false;
    }
    if (!this.state.tagList) {
      tagListError = "TagList Cannot be Empty";
    } else {
      tagListError = "";
    }
    if (tagListError) {
      this.setState({ tagListError });
      errmsg = false;
    }

    return errmsg;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const isValid = this.validat();
    if (isValid) {
      this.writePost();
    }
  };
  render() {
    if (!localStorage.getItem("app_user")) {
      return <RefreshPage></RefreshPage>;
    }
    return (
      <>
        <center>
          <AfterLoginNavBar />
          <form onSubmit={(e) => this.handleSubmit(e)}>
            <div className=" flex flex-col gap-2 lg:w-[50%] mx-8 my-auto p-4 mt-10">
              <input
                onChange={(e) => this.handleChange(e)}
                className="h-14 w-full px-5 text-[20px] border cursor-pointer border-slate-200 items-center rounded-lg hover:border-indigo-300"
                name="title"
                type="text"
                placeholder="Article Title"
                value={this.state.title}
              />
              <h2 className="text-[#bb5353]">
                {this.state.title ? "" : this.state.titleError}
              </h2>
              <input
                name="body"
                className="h-14 w-full px-5 text-[20px] border cursor-pointer border-slate-200 items-center rounded-lg hover:border-indigo-300"
                type="text"
                onChange={(e) => this.handleChange(e)}
                placeholder={`What's this article about?`}
                value={this.state.body}
              />
              <h2 className="text-[#bb5353]">
                {this.state.body ? "" : this.state.bodyError}
              </h2>
              <textarea
                name="description"
                onChange={(e) => this.handleChange(e)}
                className="h-72 w-full px-5 text-[20px] border cursor-pointer border-slate-200 items-center rounded-lg hover:border-indigo-300"
                value={this.state.description}
                placeholder={`Write your article (in markdown)`}
              ></textarea>
              <h2 className="text-[#bb5353]">
                {this.state.description ? "" : this.state.descriptionError}
              </h2>
              <input
                name="tagList"
                type="text"
                className="h-14 w-full px-5 text-[20px] border cursor-pointer border-slate-200 items-center rounded-lg hover:border-indigo-300"
                placeholder="Enter Tags"
                value={this.state.tagList}
                onChange={(e) => this.handleChange(e)}
              />
              <p className=" text-center text-2xl">
                {this.state.newPostCreated
                  ? "New Blog Created successfully! ✅"
                  : null}
              </p>
              <h2 className="text-[#bb5353]">
                {this.state.tagList ? "" : this.state.tagListError}
              </h2>
              <input
                type="submit"
                value={
                  this.state.updatingArtical ? "Updating..." : "Update Article"
                }
                className="bg-[#ffc107] px-6 py-3 text-xl rounded-lg cursor-pointer float-right self-end"
              />
            </div>
          </form>
        </center>
      </>
    );
  }
}

export default NewPost;

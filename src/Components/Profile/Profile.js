import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import AfterLoginNavBar from "../HomePageAfterLogin/AfterLoginNavbar";
import YourFeed from "../HomePage/YourFeed";
import RefreshPage from "../RefreshPage/refreshPage";
// import img from "";

class Profile extends Component {
  constructor(props) {
    super();
    this.state = {
      data: null,
      isFollowButtonClicked: false,
    };
  }
  handleButtonClick = () => {
    this.setState((prevState) => ({
      isButtonClicked: !prevState.isButtonClicked,
    }));
  };

  fechingArticlesData = () => {
    const storageKey = localStorage.token || JSON.parse(localStorage.app_user).token;
    if (storageKey) {
      const headers = {
        "Content-Type": "application/json; charset=utf-8",
        authorization: `Token ${storageKey}`,
      };
      let url = `https://api.realworld.io/api/articles?limit=10&offset=0`;
      fetch(url, {
        method: "GET",
        headers: headers,
      })
        .then((res) => res.json())
        .then((data) => {
          this.setState(() => (this.state.data = data))
          if (!localStorage.token) {
            localStorage.setItem('token', storageKey)
          }
        });
    }
  };
  componentDidMount() {
    this.fechingArticlesData();
  }
  render() {
    const isUserAvailable = localStorage.getItem("app_user");
    const dataFromLocal = isUserAvailable && JSON.parse(isUserAvailable);
    if (!dataFromLocal) {
      return <RefreshPage></RefreshPage>;
    }
    return (
      <>
        <AfterLoginNavBar />
        <div className="h-60 bg-slate-100/90 flex justify-center">
          <div className="w-3/6 flex flex-col gap-4 py-8 items-center">
            <img
              alt="Not Found"
              src={`${dataFromLocal.image}`}
              className="w-24 h-24 rounded-[50%]"
            />
            <p className="font-semibold text-2xl">{dataFromLocal.username}</p>
            <button
              className={`self-end text-sm border-[2px] py-1 px-1 ${
                this.state.isButtonClicked
                  ? "border-blue-400 text-blue"
                  : "border-gray-200 text-gray-400"
              }`}
              onClick={this.handleButtonClick}
            >
              {`+ Follow ${dataFromLocal.username}`}
            </button>
          </div>
        </div>
        <div className="flex justify-center">
          <div className="flex flex-col w-5/6 lg:w-2/5 mt-12 ">
            <h1 className="pl-5 pb-2 text-[#5cb85c] border-b-2 w-[125px] border-[#5cb85c]">
              My Articles
            </h1>
            <hr className="" />
            {/* <div className="snap-mandatory snap-y h-[1000px]">
              <div className="flex gap-2 mt-6 items-center mb-4">
                <img
                  alt="Not Found"
                  src={`${
                    dataFromLocal.user
                      ? dataFromLocal.user.image
                      : dataFromLocal.image
                  }`}
                  className="lg:w-8  lg:h-8 rounded-[50%] w-6 h-6"
                />
                <NavLink>
                  {" "}
                  <span className="font-semibold">
                    {dataFromLocal.user
                      ? dataFromLocal.user.username
                      : dataFromLocal.username}
                  </span>
                </NavLink>
              </div>
              <div className="flex gap-10">
                <div className="flex flex-col ">
                  <h4 className=" text-[16px]  lg:w-[550px] leading-5 lg:leading-8 lg:text-[22px] font-semibold ">
                    How senior product mana think differently
                  </h4>
                  <p className="text-lg text-gray-500 leading-1 hidden lg:block">
                    A deep look into product management toolkit to advance your
                    A deep look into product management toolkit to advance yourA
                    deep look into product management toolkit to advance your
                    read more....
                  </p>
                  <div className="flex mt-2 gap-2 lg:gap-4 text-slate-400 items-center text-[9px] lg:text-[16px]">
                    <p>Dec 12</p>&#x2022;
                    <p>6 min read</p>&#x2022;
                    <p className="py-[2px] px-3 bg-[#eeeee4] rounded-2xl">
                      History
                    </p>
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none">
                      <path
                        d="M12.4 12.77l-1.81 4.99a.63.63 0 0 1-1.18 0l-1.8-4.99a.63.63 0 0 0-.38-.37l-4.99-1.81a.62.62 0 0 1 0-1.18l4.99-1.8a.63.63 0 0 0 .37-.38l1.81-4.99a.63.63 0 0 1 1.18 0l1.8 4.99a.63.63 0 0 0 .38.37l4.99 1.81a.63.63 0 0 1 0 1.18l-4.99 1.8a.63.63 0 0 0-.37.38z"
                        fill="#FFC017"
                      ></path>
                    </svg>
                  </div>
                </div>
                <div className="flex">
                  <img
                    alt="Not Found"
                    src="https://images.unsplash.com/photo-1455849318743-b2233052fcff?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjl8fGJsb2dzfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
                    className="w-[150px] lg:w-[300px]"
                  />
                </div>
              </div>
            </div> */}
            <YourFeed data={this.state.data} isFromProfile={true} />
          </div>
        </div>
      </>
    );
  }
}

export default Profile;
